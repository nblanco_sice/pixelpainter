class PixelPainter {
    constructor() {
        this.me = ko.observable();
        this.text = ko.observable();
        this.users = ko.observable(1);
        this.messages = ko.observableArray([]);
        this.count = ko.computed(() => this.messages().length);
        this.canSend = ko.computed(() => this.me() && this.text());
        this._connection = new signalR.HubConnectionBuilder()
            .withUrl('http://127.0.0.1:5000/pixelpainter')
            .build();

        this._connection.on('PaintPixel', (message) => {
            //alert("PaintPixel");
            if (message.num % 1000 == 0) {
                console.log("PaintPixel " + message.num);
            }
            PixelPainter_PaintPixel(message.coord_x, message.coord_y);
        });

        this._connection.on('ClearCanvas', (message) => {
            PixelPainter_ClearCanvas();
           // alert("ClearCanvas");
        });

        this._connection.on("connected", (count) => {
            this.users(count);
        });

        this._connection.on("disconnected", (count) => {
            this.users(count);
        });

        this._connection.start().catch(error => console.error(error));
    }

    ownMessage(username) {
        return ko.computed(() => this.me() === username ? 'my-message' : 'other-message float-right');
    }

    send() {
        const message = new Message(this.me(), this.text());
        this._connection.invoke("ClearCanvas", message);
    }
}

class Message {
    constructor(username, text, num) {
        this.username = username;
        this.time = new Date();
        this.text = text;
        this.coord_x = "";
        this.coord_y = "";
        this.num = num;
    }
}

ko.applyBindings(new PixelPainter());

function PixelPainter_ClearCanvas() {
    //c = document.getElementById('canvas');
    ctx.clearRect(0, 0, 500, 200);
}

function PixelPainter_PaintPixel(x, y) {
    
    ctx.fillStyle = "rgba(0,0,0,1)";
    ctx.fillRect(x, y, 1, 1);
}

