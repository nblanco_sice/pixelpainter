﻿using ClienteNET.Models;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Threading;

namespace ClienteNET
{
    class Program
    {
        private const string _USERNAME = ".NET SignalR Client";
        private const int _MAXPIXELS = 500*200;

        static void Main(string[] args)
        {
            {
                try
                {
                    HubConnection connection = new HubConnectionBuilder()
                        .WithUrl("http://127.0.0.1:5000/pixelpainter")
                        .Build();


 

                    connection.StartAsync();

                    connection.On<Message>("GetCoord", message =>
                    {
                        Console.WriteLine("[" + message.Username + "] " + message.Text);
                    });

                    connection.On<Message>("ClearCoords", message =>
                    {
                        Console.WriteLine("[" + message.Username + "] " + message.Text);
                    });

                    connection.On<Message>("Connected", message =>
                    {
                        Console.WriteLine("[" + message.Username + "] " + message.Text);
                    });
                    connection.On<Message>("Disconnected", message =>
                    {
                        Console.WriteLine("[" + message.Username + "] " + message.Text);
                    });
                    Console.WriteLine("SingalR Client started, press key to start painting pixels!");
                    Console.ReadLine();
                    var t = DateTime.Now;
                    connection.InvokeAsync("ClearCanvas", new Message() { Username = _USERNAME });
                    for (var i=0;i< _MAXPIXELS; i++)
                    {
                        if (i % 1000 == 0) { Console.WriteLine(i); }
                        //Thread.Sleep(200);
                        connection.InvokeAsync("PaintPixel", new Message() { Username = _USERNAME, num=i });
                    }
                    TimeSpan time = DateTime.Now - t;
                    Console.WriteLine("SingalR Client finished in "+time.Seconds+"."+time.Milliseconds+" seconds.miliseconds. Key to finish");
                    Console.ReadLine();
                    connection.StopAsync();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
