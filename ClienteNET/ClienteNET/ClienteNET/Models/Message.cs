﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClienteNET.Models
{
    public class Message
    {
        public string Username { get; set; }
        public string Text { get; set; }
        public DateTime Time { get; set; }
        public int coord_x { get; set; }
        public int coord_y { get; set; }
        public int num { get; set; }
    }
}
