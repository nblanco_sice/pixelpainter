﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.Services;

namespace WebApi.Hubs
{
    public class PixelPainterHub : Hub
    {
        private static int users = 0;
        private ApplicationData _data;

        public PixelPainterHub (ApplicationData data) {
            _data = data;
            //_data.init();
        }

        public Task PaintPixel(Message message)
        {
            int num = message.num;
            var return_message = new Message();
            return_message.Username = "WebApi Central Hub";
            if (num>0 && num < _data.coord_x.Count())
            {
                return_message.num = num;
                return_message.Text = message.Username+" requested coord " + num;
                return_message.coord_x = _data.coord_x[num];
                return_message.coord_y=_data.coord_y[num];
                if (num %1000==0)
                {
                    Console.WriteLine("Painting " + num.ToString() + " pixel");
                }
            }
            
            return Clients.All.SendAsync("PaintPixel", return_message);
        }

        public Task ClearCanvas(Message message)
        {
            _data.init();
            var return_message = new Message();
            return_message.Username = "WebApi Central Hub";
            return_message.Text = "Coords data cleared by "+message.Username;
            Console.WriteLine(return_message.Text);
            return Clients.All.SendAsync("ClearCanvas", return_message);
        }

        public override Task OnConnectedAsync()
        {
            return Clients.All.SendAsync("connected", ++users);
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            return Clients.All.SendAsync("disconnected", --users);
        }

        public Task Ping(string param)
        {
            return Clients.All.SendAsync("Ping", "Hub says ping");
        }
    }
}
