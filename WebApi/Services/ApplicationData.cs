﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Services
{
    public class ApplicationData
    {
        public int[] coord_x { get; set; }

        public int[] coord_y { get; set; }

        private IConfiguration _config { get; set; }

        public ApplicationData(IConfiguration configuration)
        {
            this._config=configuration;
            init();
        }

        public void init()
        {
            Random rnd = new Random();
            int max_x = _config.GetValue<int>("coord_x_max");
            int max_y = _config.GetValue<int>("coord_y_max");
            int total_pixels = max_x * max_y;
            coord_x = new int[total_pixels];
            coord_y = new int[total_pixels];

            int[] numbers_x = Enumerable.Range(1, max_x).ToArray();
            int[] numbers_y = Enumerable.Range(1, max_y).ToArray();
            numbers_x = numbers_x.OrderBy(n => rnd.Next()).ToArray();
            numbers_y = numbers_y.OrderBy(n => rnd.Next()).ToArray();
            int cont = 0;
            for ( var i=0;i<max_x;i++)
            {
                //if (i %1000==0) { Console.WriteLine(i); }
                
                for (var j = 0; j < max_y; j++)
                {
                    coord_x[cont] = numbers_x[i];
                    coord_y[cont] = numbers_y[j];
                    cont++;
                }
            }
            coord_x = coord_x.OrderBy(n => rnd.Next()).ToArray();
            coord_y = coord_y.OrderBy(n => rnd.Next()).ToArray();
        }
    }
}
