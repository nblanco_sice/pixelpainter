﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PixelController : ControllerBase
    {
        private ApplicationData _global;

        public PixelController(ApplicationData applicationdata)
        {
            _global = applicationdata;
        }


        // GET api/pixel/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Dictionary<int,int>>> Get(int id)
        {
            //envia el próximo par de coordenadas
            var d = new Dictionary<int, int>();
            d.Add(_global.coord_x[id], _global.coord_y[id]);
            return d;
        }

        // POST api/pixel
        [HttpPost]
        public void Post()
        {
            //TODO lanzar evento de signalR para que clientes pidan un get
        }

        // DELETE api/pixel
        [HttpDelete]
        public async Task<ActionResult> Delete()
        {
            //TODO hace reset del canvas, lanzar evento signalR para que clientes vacien el canvas
            _global.init();
            return Ok();
        }

    }
}
